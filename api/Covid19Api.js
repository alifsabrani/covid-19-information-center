import fetch from "node-fetch";

export const getGlobalSummary = async () => {
  try {
    const res = await fetch("https://covid19.mathdro.id/api");
    const json = await res.json();
    return json;
  } catch (error) {
    throw new Error(error);
  }
};

export const getGlobalDailySummary = async (date) => {
  try {
    const res = await fetch(`https://covid19.mathdro.id/api/daily/${date}`);
    const json = await res.json();
    return json;
  } catch (error) {
    throw new Error(error);
  }
};

export const getSingleCaseData = async (caseType) => {
  try {
    const res = await fetch(`https://covid19.mathdro.id/api/${caseType}`);
    const json = await res.json();
    return json;
  } catch (error) {
    throw new Error(error);
  }
};
