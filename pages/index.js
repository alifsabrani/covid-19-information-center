import Head from "next/head";

import { getGlobalSummary, getGlobalDailySummary } from "../api/Covid19Api";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";

export default function Home({ data }) {
  return (
    <div>
      <Head>
        <title>Data Kasus COVID-19</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Container className="mt-5">
          <Row className="mt-3">
            <Col>
              <h1 className="text-center">Total</h1>
            </Col>
          </Row>
          <Row>
            <Col>
              <Card bg="warning">
                <Card.Header>
                  <h2>Terkonfirmasi</h2>
                </Card.Header>
                <Card.Body>
                  <Card.Text>
                    <h1>{data.total.confirmed.value}</h1>
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card bg="success" text="white">
                <Card.Header>
                  <h2>Sembuh</h2>
                </Card.Header>
                <Card.Body>
                  <Card.Text>
                    <h1>{data.total.recovered.value}</h1>
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card bg="danger" text="white">
                <Card.Header>
                  <h2>Meninggal</h2>
                </Card.Header>
                <Card.Body>
                  <Card.Text>
                    <h1>{data.total.deaths.value}</h1>
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </main>
      <style jsx>
        {`
          .text-red {
            color: #f44336;
          }
          .red {
            background-color: #f44336;
          }
          .page {
            margin-top: 2em;
          }
        `}
      </style>
    </div>
  );
}

export async function getStaticProps() {
  const total = await getGlobalSummary();
  return {
    props: { data: { total } },
  };
}
