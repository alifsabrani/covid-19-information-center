import Head from "next/head";

import { getSingleCaseData } from "../api/Covid19Api";
import GoogleMapReact from "google-map-react";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

import Marker from "../components/Marker";

export default function Map({ data }) {
  return (
    <div>
      <Head>
        <title>Penyebaran Kasus COVID-19</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Container className="mt-5">
          <Row>
            <Col>
              <h1 className="text-center">
                Peta Persebaran Kasus Positif COVID-19
              </h1>
            </Col>
          </Row>
          <Row>
            <Col>
              <div style={{ height: "100vh", width: "100%" }}>
                <GoogleMapReact
                  bootstrapURLKeys={{
                    key: "AIzaSyCcgePgoi_Vr82ZghprRc4wAP3qVm5NEZs",
                  }}
                  defaultCenter={{
                    lat: 59.95,
                    lng: 30.33,
                  }}
                  defaultZoom={0}
                >
                  {data.map((item) => {
                    return (
                      <Marker
                        lat={item.lat}
                        lng={item.long}
                        variant="primary"
                        text={
                          <div>
                            {item.countryRegion} <br /> {item.confirmed}
                          </div>
                        }
                        data={item}
                      ></Marker>
                    );
                  })}
                </GoogleMapReact>
              </div>
            </Col>
          </Row>
        </Container>
      </main>
      <style jsx>
        {`
          .text-red {
            color: #f44336;
          }
          .red {
            background-color: #f44336;
          }
          .page {
            margin-top: 2em;
          }
        `}
      </style>
    </div>
  );
}

export async function getStaticProps() {
  const data = await getSingleCaseData("confirmed");
  const hashMap = [];
  data.map((item) => {
    const current = hashMap.find(
      (el) => el.countryRegion === item.countryRegion
    );
    if (current) {
      current.confirmed =
        parseInt(current.confirmed) + parseInt(item.confirmed);
      current.deaths = parseInt(current.deaths) + parseInt(item.deaths);
      current.recovered =
        parseInt(current.recovered) + parseInt(item.recovered);
    } else {
      hashMap.push(item);
    }
  });
  return {
    props: { data: hashMap },
  };
}
