import Header from "../Header";
import Head from "next/head";
const Layout = ({ children }) => {
  return (
    <div>
      <Head>
        <link rel="stylesheet" href="css/bootstrap.min.css"></link>
      </Head>
      <Header></Header>
      {children}
    </div>
  );
};

export default Layout;
