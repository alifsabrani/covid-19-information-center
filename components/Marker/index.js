import { useState } from "react";
import Button from "react-bootstrap/Button";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";

import MarkerDetails from "./details";

const Marker = ({ variant, data, text, ...otherProps }) => {
  const [showInfo, setShowInfo] = useState(false);
  return (
    <div>
      <Button
        onClick={() => setShowInfo(true)}
        variant={variant}
        size="sm"
        {...otherProps}
      >
        {text}
      </Button>
      <MarkerDetails
        data={data}
        show={showInfo}
        handleClose={() => setShowInfo(false)}
      ></MarkerDetails>
    </div>
  );
};

export default Marker;
