import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

const MarkerDetails = ({ data, show, handleClose }) => {
  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>{data.countryRegion}</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <p>
          Terkonfirmasi :{" "}
          <strong className="text-warning">{data.confirmed}</strong> Kasus
        </p>
        <p>
          Meninggal : <strong className="text-danger">{data.deaths}</strong>{" "}
          Kasus
        </p>
        <p>
          Sembuh : <strong className="text-success">{data.recovered}</strong>{" "}
          Kasus
        </p>
      </Modal.Body>
    </Modal>
  );
};

export default MarkerDetails;
